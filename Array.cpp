// $Id: Array.cpp 820 2011-01-17 15:30:41Z hillj $

// Honor Pledge:
//
// I pledge that I have neither given nor receieved any help
// on this assignment.

#include "Array.h"
#include <stdexcept>

// Anthony, Do not import all the variables in the std:: namespace, 
// or any namespace, into the global namepace. This can result in name clashes.
// **FIXED** : Commented out namespace
//using namespace std;

// Anthony, You must implement the default constructor.
// **FIXED** : Implemented default constructor with 0 or NULL
//Default constructor.
Array::Array (void) 
	: data_(NULL),
	  cur_size_(0),
	  max_size_(0)
{

}

// Allocate required for the array and initialize cur_size_ and max_size_.
// Anthony, the Max size must be equal to the size of the memory allocation.
// **FIXED** : Initialized max_size_ to length.
Array::Array (size_t length)
	: data_(new char[length]), 
	  cur_size_(length), 
	  max_size_(length)
{

}

// Allocate the space required for the array
// and then fill the array with 'fill'.
// Anthony, the Max size must be equal to the size of the memory allocation.
// **FIXED** : Initialized max_size_ to length
Array::Array (size_t length, char fill)
	: data_(new char[length]),
	  cur_size_(length), 
	  max_size_(length)
{
	for(size_t i = 0; i < length; i++)
		this->data_[i] = fill;
}

// Anthony, The copy constructor needs to create a deep copy of the source array. 
// Otherwise, the copy will share a pointer with the original.
// When one goes out of scope, the other will eventually crash.
// **FIXED** : Fixed by allocating a new memory block and copying data to the new
// location.

// Copy constructor.
Array::Array (const Array & array)
	: data_(new char[array.cur_size_]),
	  cur_size_(array.cur_size_),
	  max_size_(array.max_size_)
{
	for(size_t i = 0; i < array.cur_size_; i++)
		this->data_[i] = array.data_[i];
}

// Deconstructor.
Array::~Array (void)
{
	delete[] data_;
}

// Anthony, The assignment operator needs to create a deep copy of the source array. 
// Otherwise, the copy will share a pointer with the original. 
// When one goes out of scope, the other will eventually crash.
// This method should return *this for call chaining.
// **FIXED** : Fixed by allocating a new memory block and copying data to the right
// memory locations. Also return *this.

// Overload operator.
// Create temporary array, copy data, return temporary array.
const Array & Array::operator = (const Array & rhs)
{
	this->cur_size_ = rhs.cur_size_;
	this->max_size_ = rhs.max_size_;
	
	for(size_t i = 0; i < rhs.cur_size_; i++)
		this->data_[i] = rhs.data_[i];
	return *this;
}

// Overload operator.
// Check if 'index' is within bounds, return char requested
// by calculating memory address.
char & Array::operator [] (size_t index)
{
	if(index > this->cur_size_ || this->cur_size_ < 0)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}

// Overload operator.
// Check if 'index' is within bounds, return char requested
const char & Array::operator [] (size_t index) const
{
	if(index > this->cur_size_ || this->cur_size_ < 0)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}

// Check if 'index' is within bounds, return char requested
char Array::get (size_t index) const
{
	if(index > this->cur_size_ || this->cur_size_ < 0)
		throw std::out_of_range("Invalid Index");
	return data_[index];
}

// Check if 'index' is within bounds, set the element requested
void Array::set (size_t index, char value)
{
	if(index > this->cur_size_ || this->cur_size_ < 0)
		throw std::out_of_range("Invalid Index");
	this->data_[index] = value;
}

// Anthony, The resize method should only increase the size of the allocation is larger than the current memory allocation for the array. 
// If resized, you need to preserve the old data.
// **FIXED** : Fixed this by first checking bounds of new_size.  If new_size is < 0 then throw exception.
// If new_size is <= max, then change the cur_size
// If new_size is > max, then create new array with max size, copy data, and destroy original.

// Check if 'new_size' is less than 'max_size_', if so,
// decrease 'cur_size_'.
void Array::resize (size_t new_size)
{
	if(new_size < 0)
		throw std::out_of_range("Invalid Index");
	else if(new_size <= this->max_size_)
		this->cur_size_ = new_size;
	else if(new_size > this->max_size_)
	{
		char *tmp = new char[new_size];
		char *ptr;
		
		for(int i = 0; i < this->cur_size_; i++)
			tmp[i] = this->data_[i];

		ptr = this->data_;
		this->data_ = tmp;
		tmp = ptr;

		this->cur_size_ = new_size;
		this->max_size_ = new_size;

		delete[] tmp;
	}
}

// Iterate through the memory locations looking for a match.
// If a match is found, return index.
// If no match is found, return -1.
int Array::find (char ch) const
{
	for(size_t i = 0; i < this->cur_size_; i++)
	{
		if(this->data_[i] == ch)
			return i;
	}
	return -1;
}

// Iterate through the memory locations looking for a match
// starting at 'start' element.
// If a match is found, return index.
// If no match is found, return -1.
int Array::find (char ch, size_t start) const
{
	if(start > this->cur_size_ || start < 0)
		throw std::out_of_range("Invalid Index");
	else
	{
		for(size_t i = start; i < this->cur_size_; i++)
		{
			if(this->data_[i] == ch)
			return i;
		}
		return -1;
	}
}

// Overload operator.
// Check if sizes of both arrays are different.
// If they're the same iterate through each element
bool Array::operator == (const Array & rhs) const
{
	if(this->cur_size_ != rhs.cur_size_)
		return false;
	else
	{
		for(size_t i = 0; i < this->cur_size_; i++)
		{
			if(this->data_[i] != rhs.data_[i])
				return false;
		}
		return true;
	}
}

// Anthony, You can define operator != in terms of operator ==.
// **FIXED** : By using the overloaded '==' operator, i reversed the expected
// return values.

// Overload operator.
// Check if sizes of both arrays are different.
// If they're the same iterate through each element
// and compare the two elements.
bool Array::operator != (const Array & rhs) const
{
	if(this->data_ == rhs.data_)
		return false;	
	return true;
}

//Fill the array with 'ch'.
void Array::fill (char ch)
{
	for(size_t i = 0; i < this->cur_size_; i++)
		this->data_[i] = ch;
}